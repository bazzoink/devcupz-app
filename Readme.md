Install cordova (phonegap):
sudo npm install -g cordova

Install ios simulator:
sudo npm install -g ios-sim

# Add plugins:
cordova plugin add org.apache.cordova.console
cordova plugin add org.apache.cordova.dialogs
cordova plugin add org.apache.cordova.splashscreen
cordova plugin add org.apache.cordova.statusbar
cordova plugin add https://github.com/Antair/Cordova-SQLitePlugin

#To serve:
cordova serve

#To use ios simulator
cordova emulate

#Always build before xcode-ing