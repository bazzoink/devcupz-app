var foodz = {};

foodz.rda_data = {
    'M': {
        'Energy': 2490,
        'Protein': 67,
        'Vitamin A': 550,
        'Vitamin C': 75,
        'Thiamin': 1.2,
        'Riboflavin': 1.3,
        'Niacin': 16,
        'Folate': 400,
        'Calcium': 750,
        'Iron': 12,
        'Magnesium': 235,
        'Phosphorus': 700,
        'Zinc': 6.4,
        'Selenium': 31,
        'Fluoride': 3.0,
        'Manganese': 2.3,
        'Vitamin D': 5,
        'Vitamin E': 12,
        'Vitamin K': 59,
        'Vitamin B-6': 1.3,
        'Vitamin B-12': 2.4,
        'Sodium': 2400,
        'Chloride': 3400,
        'Potassium': 3500,
    },
    'F': {
        'Energy': 1860,
        'Protein': 58,
        'Vitamin A': 500,
        'Vitamin C': 70,
        'Thiamin': 1.1,
        'Riboflavin': 1.1,
        'Niacin': 14,
        'Folate': 400,
        'Calcium': 750,
        'Iron': 27,
        'Magnesium': 205,
        'Phosphorus': 700,
        'Zinc': 4.5,
        'Selenium': 31,
        'Fluoride': 2.5,
        'Manganese': 1.8,
        'Vitamin D': 5,
        'Vitamin E': 12,
        'Vitamin K': 51,
        'Vitamin B-6': 1.3,
        'Vitamin B-12': 2.4,
        'Sodium': 2400,
        'Chloride': 3400,
        'Potassium': 3500,
    }
};


foodz.db = null;

foodz.app = {

	foodSearchData: [],
    dayFoodz: [],

	amountData: 0,	//100 g
	sex: '',

    foodSearchTemplate: {},
	foodContentTemplate: {},
    foodNameTemplate: {},
	calendarDatesTemplate: {},
	summaryTemplate: {},
    menuTemplate: {},

    keySort: function(arr, keyA, keyB) {
        var sortByA = function(a, b){
            var aName = a[keyA].toLowerCase();
            var bName = b[keyA].toLowerCase(); 
            return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        }
        var sortByB = function(a, b){
            var aName = a[keyB].toLowerCase();
            var bName = b[keyB].toLowerCase(); 
            return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        }
        arr.sort(sortByA);
        arr.sort(sortByB);
        return arr;
    },

    initialize: function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
        
        //templates
        this.foodSearchTemplate = Handlebars.compile($("#food-list-template").html());
		this.foodNameTemplate = Handlebars.compile($("#food-name-template").html());
		this.foodContentTemplate = Handlebars.compile($("#food-content-template").html());

        this.calendarDatesTemplate = Handlebars.compile($("#calendar-dates-template").html());
		this.summaryTemplate = Handlebars.compile($("#day-summary-template").html());
        this.menuTemplate = Handlebars.compile($("#day-menu-template").html());

        //localstorage
        this.sex = window.localStorage.getItem('sex') || 'M';
    },

    onDeviceReady: function() {
		FastClick.attach(document.body);
        window.sqlitePlugin.importPrepopulatedDatabase({file: "foodz.db"});
        foodz.db = window.sqlitePlugin.openDatabase({name: "foodz.db"});
        //window.localStorage.removeItem('summary');
    },

    homeBeforeCreate: function(event, args) {
    	//Search Page
    	var app = this;

        $("#input-sex").val(app.sex);
    	$("#food-search").submit(function(e){
    		e.preventDefault();
    		if ($("input[name='q']").val() == '' || $("input[name='amt']").val() == ''){
                navigator.notification.alert("Fill up all values", function(){return}, "Oops!");
    			return false;
    		}
            app.amountData = $('input[name="amt"]').val();
            var searchData = $('input[name="q"]').val();
            foodz.db.executeSql(
                //'SELECT * from foods_food WHERE description LIKE "%' + searchData + '%";',
                'SELECT * from foods_food WHERE description REGEXP "[[:<:]]' + searchData + '(,?)[[:>:]]";',
                [],
                function(results){
                    var resultsArray = [];
                    for (var i = 0; i < results.rows.length; i++) {
                        resultsArray.push(results.rows.item(i));
                    }
                    app.foodSearchData = app.keySort(resultsArray, 'description', 'group');
                    $(':mobile-pagecontainer').pagecontainer('change', '#food-search', {transition: 'slide'});
                    $("#food-search input").val('');
                    return false;
                },
                function(err){
                    navigator.notification.alert("Try again later", function(){return}, "Database Error");
                }
            )
    	});
    	$("#input-sex").off('change').on('change', function(e){
    		app.sex = $(this).val();
            window.localStorage.setItem('sex', app.sex);
    	});
    },

    foodSearchBeforeShow: function(event, args) {
    	//Search Page
        var app = this;
    	var searchResults = app.foodSearchData;
    	$("#food-list")
            .html(app.foodSearchTemplate(searchResults))
        	.listview({
        		autodividers:true,
        		autodividersSelector: function ( li ) {
        			return li.data("group");
        		},
        	})
            .enhanceWithin()
            .listview('refresh');
    },

	foodBeforeShow: function(event, args) {
        $("#add-button").off('click');
		var app = this;
		var curFood = app.foodSearchData[args[1]];
        curFood['amount'] = app.amountData;

        foodz.db.executeSql(
            'SELECT * FROM foods_nutrient WHERE food_id=' + curFood.id + ';',
            [],
            function(results){
                var resultsArray = [];
                for (var i = 0; i < results.rows.length; i++) {
                    resultsArray.push(results.rows.item(i));
                }
                var nutrients = app.keySort($.map(resultsArray, function(n){
                    return {
                        nuKey: n.description, 
                        nuVal: (n.value * (app.amountData/100)).toFixed(2), 
                        group: n.group,
                        units: n.units,
                    }
                }), 'nuKey', 'group');

                $("#food-content")
                    .html(app.foodContentTemplate(nutrients))
                    .listview({
                        autodividers:true,
                        autodividersSelector: function ( li ) {
                            return li.data("group");
                        },
                    })
                    .enhanceWithin()
                    .listview('refresh');

                $("#food-name")
                    .html(app.foodNameTemplate(curFood))
                    .enhanceWithin();

                //bind
                $("#add-button").on('click', function(e){
                    var summary = JSON.parse(window.localStorage.getItem('summary') || '{}');
                    var today = Date.now();
                    summary[today] = {
                        nutrients: nutrients,
                        food: curFood,
                    }
                    window.localStorage.setItem('summary', JSON.stringify(summary));

                    navigator.notification.alert("Added to foodz!", function(){
                        $(':mobile-pagecontainer').pagecontainer('change', '#summary?' + today, {transition: 'slide'});
                        return false;
                    }, "Yummeh");
                    return false;
                });
                return false;
            },
            function(err){
                navigator.notification.alert("Try again later", function(){return}, "Database Error");
            }
        )
	},


    calendarBeforeShow: function(event, args) {
        var app = this;
        var summary = JSON.parse(window.localStorage.getItem('summary') || '{}');
        var dates = [];

        //unique dates
        for (var key in summary){
            //check within date (key)
            var keyDate = new Date(key*1).toDateString();
            if (dates.indexOf(keyDate) <= -1)
                dates.push(keyDate);
        }
        dates = $.map(dates, function(n){return {
                'date': n,
                'timestamp': Date.parse(n), 
            }; 
        }).sort(function(a,b){
            return ((a.timestamp > b.timestamp) ? -1 : ((a.timestamp < b.timestamp) ? 1 : 0));
        });

        $("#calendar-dates")
            .html(app.calendarDatesTemplate(dates))
            .enhanceWithin()
            .listview('refresh');
    },
    
    summaryBeforeShow: function(event, args) {
        //get data for provided date!
    	var app = this;
        var summary = JSON.parse(window.localStorage.getItem('summary') || '{}');

        var todayString = new Date(args[1]*1).toDateString();
        var descVal = {};
        var descGroup = {};

        //get data from localstorage!
        app.dayFoodz = [];
        for (var key in summary){
            //check within date (key)
            var keyDate = new Date(key*1).toDateString();
            if (todayString == keyDate ) {
                var dayFoodz = summary[key]['food'];
                dayFoodz['timestamp'] = key;
                app.dayFoodz.push(dayFoodz);
                var timeNutrients = summary[key]['nutrients'];
                for (var i = 0; i < timeNutrients.length; i++) {
                    var desc = timeNutrients[i]['nuKey'];
                    var val = parseFloat(timeNutrients[i]['nuVal']);
                    var group = timeNutrients[i]['group'];

                    descVal[desc] = (descVal[desc] || 0) + val;
                    descGroup[desc] = group;
                }
            }
        }

        //nutrients display
        var nutrients = app.keySort($.map(descVal, function(val, key){
            var target = foodz.rda_data[app.sex][key];
            return {
                nuKey: key,
                nuVal: (100*val/target).toFixed(2),
                group: descGroup[key],
                style: val < target ? "background:#ffa0a0" : "background:#d1d6fc",
            }
        }), 'nuKey', 'group');

    	$("#day-summary")
            .html(app.summaryTemplate(nutrients))
            .listview({
        		autodividers:true,
        		autodividersSelector: function ( li ) {
        			return li.data("group");
        		},
        	})
            .enhanceWithin()
            .listview('refresh');
    },

    menuBeforeShow: function(event, args) {
        //Search Page
        var app = this;
        var menuList = app.dayFoodz.sort(function(a,b){
            return ((a.timestamp > b.timestamp) ? -1 : ((a.timestamp < b.timestamp) ? 1 : 0));
        });

        $("#day-menu")
            .html(app.menuTemplate(menuList))
            .enhanceWithin()
            .listview('refresh');

        $(".delete-food").off("click").on("click", function(){
            var food = $(this).parent("li").data("timestamp");
            navigator.notification.confirm("Delete food?", function(index){
                if (index==1){
                    var summary = JSON.parse(window.localStorage.getItem('summary') || '{}');
                    delete summary[food];
                    window.localStorage.setItem('summary', JSON.stringify(summary));
                    history.back();
                    return false;
                }
            }, "Deletez");
        });
    },

};

foodz.router = new $.mobile.Router(
	{
		"#home$": {handler: "homeBeforeCreate", events:"bc"},
		"#food-data[?](\\d+)$": {handler: "foodBeforeShow", events:"bs"},
		"#food-search$": {handler: "foodSearchBeforeShow", events:"bs"},

        "#calendar$": {handler: "calendarBeforeShow", events:"bs"},
		"#summary[?](\\d+)$": {handler: "summaryBeforeShow", events:"bs"},
        "#menu$": {handler: "menuBeforeShow", events:"bs"},

	},
	foodz.app
);